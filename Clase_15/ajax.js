//ajax

let datos;
let URL = "https://jsonplaceholder.typicode.com/users";

/*
let xhr = new XMLHttpRequest();

xhr.onreadystatechange = function () {
    console.log("Estados",xhr.readyState);
    if (xhr.readyState == 4) {
        //console.log(xhr.responseText);
        datos = JSON.parse(xhr.responseText);
        console.log(datos);
    }
};

xhr.open("GET",URL);
xhr.send();
*/

//jquery


$.get(URL, (data) => {
    console.log(data);
});


//AXIOS

/*
axios.get (URL)
.then(function (dato) {
    console.log(dato);
})
.catch(error => console.error()
)
*/

//fetch

fetch (URL)
.then(function (respuesta) {
    //console.log(data);
    return respuesta.json();
}).then((data) => {
    console.log(data);
})

