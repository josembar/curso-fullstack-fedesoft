// 1. Elabore unna funcion que imprima los numeros divisibles
// por 5 y 3 entre 5 y 50

const funcion1 = "Números divisibles por 5 y 3 entre 5 y 50";

function imprimirDiv5y3 ()
{
    const msjDiv5y3 = "Número divisible por 5 y 3:";
    for (let index = 5; index <= 50; index++) 
    {
        if (index % 5 == 0 && index % 3 == 0)
        {
            console.log(`${msjDiv5y3}`,index);
        }
    }
}

console.log(funcion1);
imprimirDiv5y3();


// 2. Elabore una funcion que imprima
// el siguiente arreglo [3,6,2,5] de
// al reves

const funcion2 = "Arreglo al revés";
let arreglo = [3,6,2,5];

function arregloReves (matriz)
{
    //let arreglo = [3,6,2,5];
    console.log("Arreglo inicial:",matriz);
    let arregloR = matriz.reverse();
    console.log("Arreglo al revés",arregloR);
}

console.log(funcion2);
arregloReves(arreglo);


// 3. Elabore una funcion que sume todos
// los elementos de un arreglo y retorne
// el resultado [3,6,2,5]

const funcion3 = "Suma de elementos de un arreglo";

function sumaElementosArreglo (matriz)
{
    //let arregloS = [3,6,2,5];
    let suma = 0;
    console.log("Arreglo:",matriz);
    for (let index = 0; index < matriz.length; index++) 
    {
        suma += matriz[index];
    }
    //console.log("La suma de los elementos es:",suma);
    return suma;
}

console.log(funcion3);
console.log("La suma de los elementos es:",sumaElementosArreglo(arreglo));

// 4. Elabore una funcion que retorne el
// maximo numero del siguiente arreglo [3,6,2,5]

const funcion4 = "Máximo elemento de un arreglo";

function maxElementoArreglo (matriz) 
{
    //let arregloM = [3,6,2,5];
    console.log("Arreglo:",matriz);
    let maxElemento = Math.max(...matriz);
    return maxElemento;
}

console.log(funcion4);
console.log("Máximo número:",maxElementoArreglo(arreglo));

// 5. Elabore una funcion que reciba un numero y retorne por medio
// de la consola si es impar

const funcion5 = "Número impar";
const promptImpar = "Ingrese un número";

function numeroImpar (n)
{
    //let a = n;
    let msjImpar;
    if (n % 2 == 0) 
    {
        msjImpar = "número par";
    }

    else
    {
        msjImpar = "número impar";
    }
    return `${n} ${msjImpar}`;
}

//let numero = parseInt(prompt(`${promptImpar}`));
console.log(funcion5);
//console.log(numeroImpar(numero));
console.log(numeroImpar(8));

/*******************/

let numeros = [ 4, -9, 3, -1, 8, 10, 12, 8, -7, 20 ];

// 6. Elabore una funcion que retorne la cantidad de numeros negativos que
// tiene un arreglo

const funcion6 = "Camtidad de número negativos en un arreglo";

function totalNumerosNegativos (matriz)
{
    let contador = 0;
    console.log("Arreglo:",matriz);
    for (let index = 0; index < matriz.length; index++)
    {
        if (matriz[index] < 0)
        {
            contador++;
        }
    }
    return contador;
}

console.log(funcion6);
console.log("Total números negativos",totalNumerosNegativos(numeros));

let nombre = 'CARLOS';
let apellido = 'Perez'
let edad;
let anioNacimiento = 1991;

let datos = [ {
    userId: 1,
    id: 1,
    title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto",
  },
  {
    userId: 1,
    id: 2,
    title: "qui est esse",
    body: "est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla",
  },
  {
    userId: 1,
    id: 3,
    title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
    body: "et iusto sed quo iure voluptatem occaecati omnis eligendi aut ad voluptatem doloribus vel accusantium quis pariatur molestiae porro eius odio et labore et velit aut",
  },
  {
    userId: 1,
    id: 4,
    title: "eum et est occaecati",
    body: "ullam et saepe reiciendis voluptatem adipisci sit amet autem assumenda provident rerum culpa quis hic commodi nesciunt rem tenetur doloremque ipsam iure quis sunt voluptatem rerum illo velit",
  },
  {
    userId: 1,
    id: 5,
    title: "nesciunt quas odio",
    body: "repudiandae veniam quaerat sunt sed alias aut fugiat sit autem sed est voluptatem omnis possimus esse voluptatibus quis est aut tenetur dolor neque",
  },
  {
    userId: 1,
    id: 6,
    title: "dolorem eum magni eos aperiam quia",
    body: "ut aspernatur corporis harum nihil quis provident sequi mollitia nobis aliquid molestiae perspiciatis et ea nemo ab reprehenderit accusantium quas voluptate dolores velit et doloremque molestiae",
  },
  {
    userId: 1,
    id: 7,
    title: "magnam facilis autem",
    body: "dolore placeat quibusdam ea quo vitae magni quis enim qui quis quo nemo aut saepe quidem repellat excepturi ut quia sunt ut sequi eos ea sed quas",
  },
  {
    userId: 1,
    id: 8,
    title: "dolorem dolore est ipsam",
    body: "dignissimos aperiam dolorem qui eum facilis quibusdam animi sint suscipit qui sint possimus cum quaerat magni maiores excepturi ipsam ut commodi dolor voluptatum modi aut vitae",
  }
]

// 7. Elabore una funcion que retorne los id impares retornando un array

const funcion7 = "id's impares de un objeto";

function idsImpares (data)
{
    let imparesId = [];
    for (let index = 0; index < data.length; index++) 
    {
        let idI = data[index].id;
        if (idI % 2 != 0) 
        {   
            imparesId.push(idI);
        }
    }
    return imparesId;
}

/*
function idsImpares2 (data)
{
    let imparesId = [];
    data.forEach(element => {
        
    });
    console.log(imparesId);
    return true;   
}
*/

console.log(funcion7);
console.log("idsImpares:",idsImpares(datos));
//console.log("idsImpares2",idsImpares2(datos));

// 8. Elabore una funcion que retorne el factorial de un numero no olvide que el factorial
// es factorial(5) => 0x1x2x3x4x5 = 120

const funcion8 = "Factorial de un número";

function factorial (n)
{
    let inicio = 1;
    for (let index = 2; index <= n; index++) 
    {
        inicio = inicio * index;
    }
    return `Factorial de ${n}: ${inicio}`;
    //return inicio
}

console.log(funcion8);
//let numero2 = parseInt(prompt(`${promptImpar}`));
//console.log(factorial(numero2));
console.log(factorial(6));

// 9 cree un ciclo o bucle que me imprima todos los numeros entre -10 y 19

const funcion9 = "Números desde -10 a 19";

function numeros_n10_19 ()
{
    const nO = "Número:";
    for (let index = -10; index <= 19; index++)
    {
        console.log(`${nO} ${index}`);
    }
    return true;
}

//console.log(funcion9);
//console.log(numeros_n10_19());

// 10 cree un ciclo o bucle que me imprima todos los numeros pares entre 2 y 40

const funcion10 = "Números pares entre 2 y 40";

function numerosPares2_40 ()
{
    const nP = "Número par:";
    for (let index = 2; index <= 40; index++) 
    {
        if (index % 2 == 0)
        {
            console.log(`${nP} ${index}`);
        }
    }
    return true;
}

//console.log(funcion10);
//console.log(numerosPares2_40());

// 11 cree un ciclo o bucle que me imprima todos los numeros impares entre 300 y 333

const funcion11 = "Números impares entre 300 y 333";

function numerosImpares300_333 ()
{
    const nI = "Número impar:";
    for (let index = 300; index <= 333; index++) 
    {
        if (index % 2 != 0)
        {
            console.log(`${nI} ${index}`);
        }    
    }
    return true;
}

//console.log(funcion11);
//console.log(numerosImpares300_333());