let persona = 
{
    nombre: "María",
    apellido: "Lisboa",
    imprimirNombre: function()
    {
        //console.log("Contexto Nombre: ",this);
        console.log(`${this.nombre} ${this.apellido}`);
    },
    direccion:
    {
        pais: "Colombia",
        ciudad: "Bogotá",
        obtenerDir: function()
        {
            //console.log("Contexto Dirección: ",this);
            //console.log(`${this.pais} - ${this.ciudad}`);
            console.log(this);
            let self = this;
            let nuevaDir = function () 
            {
                console.log(self);
            };

            nuevaDir();
        }
    }
};

//console.log("Objeto global: ",this);
//console.log("Persona: ",persona);
//console.log("Contexto this",persona.imprimirNombre());
//persona.imprimirNombre();
persona.direccion.obtenerDir();