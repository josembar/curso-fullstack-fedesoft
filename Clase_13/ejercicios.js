let edad = 25;

function calcularDias (age)
{
    let dias = age * 365;
    return dias;
}

console.log(`Edad: ${calcularDias(edad)}`);

let n1 = 5;
let n2 = 8;

function multiplica (x,y)
{
    let resultado = x * y;
    return resultado;
}

console.log(`Multiplicación: ${multiplica(n1,n2)}`);

var cadena1 = "José";
var cadena2 = "Barrantes";

function concatenar (str1,str2)
{
    let sizeCadena1 = str1.length;
    let sizeCadena2 = str2.length;
    let concCadena3 = str1 + str2;
    let todo = `Tamaño cadena 1 = ${sizeCadena1}, tamaño cadena 2 = ${sizeCadena2}, cadenas unidas: ${concCadena3}`;
    return todo;
}

console.log(concatenar(cadena1,cadena2));

function mayorQue (x,y)
{
    let retorno = new Boolean();
    let mensajeResultado;
    if (x > y || y > x)
    {
        retorno = true; 
        mensajeResultado = "Uno de los números es mayor";
    }

    if (x == y)
    {
        retorno = false;
        mensajeResultado = "Ninguno de los números es mayor";
    }
    return `${retorno}, ${mensajeResultado}`;
}

console.log(mayorQue(n1,n2));
//console.log(mayorQue(2,2));

function nMenor(x,y) 
{
    let menor;
    let mResultado;
    if (x !== y)
    {
        menor = Math.min(x,y);
        mResultado = "El número menor es:";
    }
    
    else 
    {
        menor = x;
        mResultado = "Números iguales. Ambos números valen";
    }
    
    return `${mResultado} ${menor}`;
}

console.log(nMenor(n1,n2));
//console.log(nMenor(2,2));

function nombreReves(name)
{
    let nameReves = name.split("").reverse().join("").toLowerCase();
    const msj = "El nombre al revés es:"
    return `${msj} ${nameReves}`;
}

console.log(nombreReves(cadena1));

function apellidoMys(lastName)
{
    let apellido = lastName.toUpperCase();
    const msg = "El apellido es:";
    return `${msg} ${apellido}`;
}

console.log(apellidoMys(cadena2));

function calcularEdad(anioNac)
{
    const msjEdad = "La edad es:";
    let anioActual = new Date().getFullYear();
    let edadA = anioActual - anioNac;
    return `${msjEdad} ${edadA}`;
}

let anio = 1993;
console.log(calcularEdad(anio));




