let persona = function (nombre)
{
    this.name = nombre,
    this.pv = 100,
    this.sp = 100,
    this.curar = function(person)
    {
        //person.pv = person.pv + 20;
        console.log(`${this.name} ---> Curar`);
        person.pv += 20;
        //this.sp = this.sp - 40;
        this.sp -= 40;
        console.log(`${person.name} ha quedado con ${person.pv} puntos de vida`);
        console.log(`${this.name} ha quedado con ${this.sp} puntos de magia `);
        this.estado();
        person.estado();
        //console.log("Persona curada: " + person.estado());
    }
    this.atacar = function(person)
    {
        console.log(`${this.name} ---> Atacar`);
        //person.pv = person.pv + 20;
        person.pv -= 40;
        //this.sp = this.sp - 40;
        person.sp -= 20;
        console.log(`${person.name} ha quedado con ${person.pv} puntos de vida`);
        console.log(`${person.name} ha quedado con ${person.sp} puntos de magia `);
        person.estado();
        this.estado();
    }

    let self = this;
    
    this.estado = function ()
    {
        console.log(self);
    }
    
}

let gandalft = new persona("gandalft");
let legolas = new persona("legolas");

console.log(gandalft);
console.log(legolas);