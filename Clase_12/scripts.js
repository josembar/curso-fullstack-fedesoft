//let arreglo = [12,true,{nombre: "Pedro"},"sumar"];
let arreglo = [12,5,18,90,25];

let arreglodos = new Array();

let arreglotres = [];

//Forma 1
/*
arreglofinal = arreglo.map(function(numero){
    return numero * numero;
})

console.log(arreglofinal);
*/

//Forma 2
/*
arreglofinal = arreglo.map(function(numero){
    return numero * numero;
})


console.log(arreglofinal);
*/
//Arrow functions

/*
let arreglofinal = arreglo.map(numero =>
{
    return numero * numero;
})

console.log(`Arrow function ${arreglofinal}`);
*/

// arrow function una sola línea

let arreglofinal = arreglo.map(numero => numero * numero)
    
    //console.log(`Arrow function una línea ${arreglofinal}`);
    console.log(arreglofinal);

// convertir arreglo a String

let textoArreglo = arreglo.join(";");
console.log(arreglo);
console.log("String: " + textoArreglo)
console.log("ToString", arreglo.toString());

let textoUno = "Hola mundo cruel";
let arregloText = textoUno.split(" ");
console.log(arregloText);

//Adicionar elementos a un arreglo en la última posición

//arreglo.push(100);
//console.log("Nuevo elemento", arreglo);

//Agrear elemento al inicio
arreglo.unshift(100);
console.log("Elemento al inicio",arreglo);

//eliminar último elemento
//console.log(arreglo.pop());
let pop = arreglo.pop()
console.log(arreglo);
console.log(pop);

//for each
/*
arreglo.forEach(elemento => 
{
    console.log(elemento);
})
*/

//Filtrar elementos de un arreglo

let arreglito = arreglo.filter(function(elemento)
{
    return elemento >= 18;
})

console.log("Arreglito",arreglito);

//Filtrar un elemento

let elemento = arreglo.find(function(el)
{
    return el == 12;
})

console.log("Elemento filtrado", elemento);

//operador spread ..., une 2 o más arreglos

let resultado = [...arreglo,...arreglofinal,...[1,2,3,4]];
console.log("Resultado", resultado);