//Timeout
/*
setTimeout(function () {
    console.log("Soy un setTimeout");
    clearInterval(intervalo);
},6000)

let intervalo = setInterval(impresion,2000);

function impresion() 
{
    console.log("hola");
}
*/

//promesas

function paso1()
{
    return new Promise( function (resolve,reject) {
        //resolve();
        //reject();
        setTimeout(() => {
            resolve("Paso 1");
        }, 1500);
    }) 
}

function paso2(segundoDato)
{
    return new Promise(function (resolve,reject) {
        setTimeout(() => {
            resolve([true,segundoDato]);
        }, 500); 
    })
        
}

function paso3(datoFinal)
{
    return new Promise(function (resolve,reject) {
        setTimeout(() => {
            resolve(["Paso 3",datoFinal]);
        }, 200);  
    })
      
}

//paso1(); paso2(); paso3();
/*
paso1()
.then(function (dato) {
    console.log(dato);
})

paso2()
.then(function (dato) {
    console.log(dato);
})

paso3()
.then(function (dato) {
    console.log(dato);
})
*/

//async - await
/*
let pasosTotales = async function () {

    let p1 = await paso1();
    let p2 = await paso2(p1);
    let p3 = await paso3(p2);
    
    console.log(p1);
    console.log(p2);
    console.log(p3);
}
*/

//pasosTotales();

let usuarios = [
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    },
    {
      "id": 2,
      "name": "Ervin Howell",
      "username": "Antonette",
      "email": "Shanna@melissa.tv",
      "address": {
        "street": "Victor Plains",
        "suite": "Suite 879",
        "city": "Wisokyburgh",
        "zipcode": "90566-7771",
        "geo": {
          "lat": "-43.9509",
          "lng": "-34.4618"
        }
      },
      "phone": "010-692-6593 x09125",
      "website": "anastasia.net",
      "company": {
        "name": "Deckow-Crist",
        "catchPhrase": "Proactive didactic contingency",
        "bs": "synergize scalable supply-chains"
      }
    },
    {
      "id": 3,
      "name": "Clementine Bauch",
      "username": "Samantha",
      "email": "Nathan@yesenia.net",
      "address": {
        "street": "Douglas Extension",
        "suite": "Suite 847",
        "city": "McKenziehaven",
        "zipcode": "59590-4157",
        "geo": {
          "lat": "-68.6102",
          "lng": "-47.0653"
        }
      },
      "phone": "1-463-123-4447",
      "website": "ramiro.info",
      "company": {
        "name": "Romaguera-Jacobson",
        "catchPhrase": "Face to face bifurcated interface",
        "bs": "e-enable strategic applications"
      }
    }
  ];

  let $divPrincipal = document.getElementById("content-data");

  let $divInterno = document.createElement("div");
  $divInterno.className = "div mr-3 ml-3";

  let $ul = document.createElement("ul");
  $ul.className = "list-group";

  let $li = document.createElement("li");
  $li.className = "list-group-item";
  $li.innerText = "José";

  $ul.appendChild($li);
  $divInterno.appendChild($ul);
  $divPrincipal.appendChild($divInterno);
